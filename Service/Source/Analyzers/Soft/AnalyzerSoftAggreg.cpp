// This file is part of openZmeter.
//
// openZmeter is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// openZmeter is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with openZmeter.  If not, see <https://www.gnu.org/licenses/>
// ---------------------------------------------------------------------------------------------------------------------
#include "AnalyzerSoft.h"
// ---------------------------------------------------------------------------------------------------------------------
AnalyzerSoftAggreg::AnalyzerSoftAggreg(const string &serial, const Analyzer::AnalyzerMode mode) : pSerial(serial), pVoltageChannels(Analyzer::VoltageChannels(mode)), pCurrentChannels(Analyzer::CurrentChannels(mode)) {
  Reset();
}
AnalyzerSoftAggreg::~AnalyzerSoftAggreg() {
}
uint64_t AnalyzerSoftAggreg::Timestamp() const {
  return pTimestamp;
}
float AnalyzerSoftAggreg::Frequency() const {
  return pFrequency;
}
bool AnalyzerSoftAggreg::Flag() const {
  return pFlag;
}
float AnalyzerSoftAggreg::Unbalance() const {
  return pUnbalance;
}
float AnalyzerSoftAggreg::Voltage(const uint8_t channel) const {
  return pVoltage[channel];
}
float AnalyzerSoftAggreg::Current(const uint8_t channel) const {
  return pCurrent[channel];
}
float AnalyzerSoftAggreg::ActivePower(const uint8_t channel) const {
  return pActive_Power[channel];
}
float AnalyzerSoftAggreg::ApparentPower(const uint8_t channel) const {
  return pApparent_Power[channel];
}
float AnalyzerSoftAggreg::ReactivePower(const uint8_t channel) const {
  return pReactive_Power[channel];
}
float AnalyzerSoftAggreg::ActiveEnergy(const uint8_t channel) const {
  return pActive_Energy[channel];
}
float AnalyzerSoftAggreg::ReactiveEnergy(const uint8_t channel) const {
  return pReactive_Energy[channel];
}
float AnalyzerSoftAggreg::VoltageHarmonic(const uint8_t channel, const uint8_t harmonic) const {
  return pVoltage_Harmonics[channel * 50 + harmonic];
}
float AnalyzerSoftAggreg::CurrentHarmonic(const uint8_t channel, const uint8_t harmonic) const {
  return pCurrent_Harmonics[channel * 50 + harmonic];
}
float AnalyzerSoftAggreg::PowerHarmonic(const uint8_t channel, const uint8_t harmonic) const {
  return pPower_Harmonics[channel * 50 + harmonic];
}
float AnalyzerSoftAggreg::CurrentTHD(const uint8_t channel) const {
  return pCurrent_THD[channel];
}
float AnalyzerSoftAggreg::VoltageTHD(const uint8_t channel) const {
  return pVoltage_THD[channel];
}
float AnalyzerSoftAggreg::VoltagePhase(const uint8_t channel) const {
  return pVoltage_Phase[channel];
}
float AnalyzerSoftAggreg::Phi(const uint8_t channel) const {
  return pPhi[channel];
}
float AnalyzerSoftAggreg::PowerFactor(const uint8_t channel) const {
  return pPower_Factor[channel];
}
void AnalyzerSoftAggreg::Reset() {
  pFlag      = false;
  pFrequency = 0;
  pUnbalance = 0;
  pTimestamp = 0;
  memset(pVoltage, 0, sizeof(pVoltage));
  memset(pCurrent, 0, sizeof(pCurrent));
  memset(pActive_Power, 0, sizeof(pActive_Power));
  memset(pReactive_Power, 0, sizeof(pReactive_Power));
  memset(pActive_Energy, 0, sizeof(pActive_Energy));
  memset(pReactive_Energy, 0, sizeof(pReactive_Energy));
  memset(pApparent_Power, 0, sizeof(pApparent_Power));
  memset(pPower_Factor, 0, sizeof(pPower_Factor));
  memset(pVoltage_THD, 0, sizeof(pVoltage_THD));
  memset(pCurrent_THD, 0, sizeof(pCurrent_THD));
  memset(pVoltage_Phase, 0, sizeof(pVoltage_Phase));
  memset(pPhi, 0, sizeof(pPhi));
  memset(pVoltage_Harmonics, 0, sizeof(pVoltage_Harmonics));
  memset(pCurrent_Harmonics, 0, sizeof(pCurrent_Harmonics));
  memset(pPower_Harmonics, 0, sizeof(pPower_Harmonics));
}
void AnalyzerSoftAggreg::Accumulate(const AnalyzerSoftAggreg *src) {
  pFrequency += src->pFrequency;
  pUnbalance += src->pUnbalance;
  pFlag      |= src->pFlag;
  for(uint8_t n = 0; n < pVoltageChannels; n++)        pVoltage[n] += src->pVoltage[n] * src->pVoltage[n];
  for(uint8_t n = 0; n < pCurrentChannels; n++)        pCurrent[n] += src->pCurrent[n] * src->pCurrent[n];
  for(uint8_t n = 0; n < pVoltageChannels; n++)        pActive_Power[n] += src->pActive_Power[n];
  for(uint8_t n = 0; n < pVoltageChannels; n++)        pReactive_Power[n] += src->pReactive_Power[n];
  for(uint8_t n = 0; n < pCurrentChannels; n++)        pCurrent_THD[n] += src->pCurrent_THD[n] * src->pCurrent_THD[n];
  for(uint8_t n = 0; n < pVoltageChannels; n++)        pPhi[n] += src->pPhi[n];
  for(uint8_t n = 0; n < pVoltageChannels; n++)        pPower_Factor[n] += src->pPower_Factor[n];
  for(uint8_t n = 0; n < (50 * pCurrentChannels); n++) pCurrent_Harmonics[n] += src->pCurrent_Harmonics[n] * src->pCurrent_Harmonics[n];
  for(uint8_t n = 0; n < (50 * pVoltageChannels); n++) pVoltage_Harmonics[n] += src->pVoltage_Harmonics[n] * src->pVoltage_Harmonics[n];
  for(uint8_t n = 0; n < (50 * pVoltageChannels); n++) pPower_Harmonics[n] += src->pPower_Harmonics[n];
  for(uint8_t n = 0; n < pVoltageChannels; n++)        pVoltage_THD[n] += src->pVoltage_THD[n] * src->pVoltage_THD[n];
  for(uint8_t n = 0; n < pVoltageChannels; n++)        pActive_Energy[n] += src->pActive_Energy[n];
  for(uint8_t n = 0; n < pVoltageChannels; n++)        pVoltage_Phase[n] += src->pVoltage_Phase[n];
  for(uint8_t n = 0; n < pVoltageChannels; n++)        pReactive_Energy[n] += src->pReactive_Energy[n];
  for(uint8_t n = 0; n < pVoltageChannels; n++)        pApparent_Power[n] += src->pApparent_Power[n];
}
void AnalyzerSoftAggreg::Normalize(const uint16_t samples) {
  pFrequency = pFrequency / samples;
  pUnbalance = pUnbalance / samples;
  for(uint8_t n = 0; n < pVoltageChannels; n++)        pVoltage[n] = sqrt(pVoltage[n] / samples);
  for(uint8_t n = 0; n < pCurrentChannels; n++)        pCurrent[n] = sqrt(pCurrent[n] / samples);
  for(uint8_t n = 0; n < pVoltageChannels; n++)        pActive_Power[n] = pActive_Power[n] / samples;
  for(uint8_t n = 0; n < pVoltageChannels; n++)        pReactive_Power[n] = pReactive_Power[n] / samples;
  for(uint8_t n = 0; n < pCurrentChannels; n++)        pCurrent_THD[n] = sqrt(pCurrent_THD[n] / samples);
  for(uint8_t n = 0; n < pVoltageChannels; n++)        pPhi[n] = pPhi[n] / samples;
  for(uint8_t n = 0; n < pVoltageChannels; n++)        pPower_Factor[n] = pPower_Factor[n] / samples;
  for(uint8_t n = 0; n < (50 * pCurrentChannels); n++) pCurrent_Harmonics[n] = sqrt(pCurrent_Harmonics[n] / samples);
  for(uint8_t n = 0; n < (50 * pVoltageChannels); n++) pVoltage_Harmonics[n] = sqrt(pVoltage_Harmonics[n] / samples);
  for(uint8_t n = 0; n < (50 * pVoltageChannels); n++) pPower_Harmonics[n] = pPower_Harmonics[n] / samples;
  for(uint8_t n = 0; n < pVoltageChannels; n++)        pVoltage_THD[n] = sqrt(pVoltage_THD[n] / samples);
  for(uint8_t n = 0; n < pVoltageChannels; n++)        pVoltage_Phase[n] = pVoltage_Phase[n] / samples;
  for(uint8_t n = 0; n < pVoltageChannels; n++)        pApparent_Power[n] = pApparent_Power[n] / samples;
}
void AnalyzerSoftAggreg::muParserInit(Parser &parser, const string &expression) {
  auto AppendArray = [&](const string &serie, float *data, const uint8_t len) {
    if(len == 1) {
      parser.DefineVar(serie, &data[0]);
    } else {
      for(uint8_t n = 0; n < len; n++) parser.DefineVar(serie + "_" + to_string(n + 1), &data[n]);
    }
  };
  parser.DefineVar("Frequency", &pFrequency);
  AppendArray("Voltage", pVoltage, pVoltageChannels);
  AppendArray("Current", pCurrent, pCurrentChannels);
  AppendArray("Active_Power", pActive_Power, pVoltageChannels);
  AppendArray("Reactive_Power", pReactive_Power, pVoltageChannels);
  AppendArray("Apparent_Power", pApparent_Power, pVoltageChannels);
  AppendArray("Power_Factor", pPower_Factor, pVoltageChannels);
  AppendArray("Voltage_THD", pVoltage_THD, pVoltageChannels);
  AppendArray("Current_THD", pCurrent_THD, pCurrentChannels);
  AppendArray("Phi", pPhi, pVoltageChannels);
  if(pVoltageChannels > 1) {
    AppendArray("Voltage_Phase", pVoltage_Phase, pVoltageChannels);
    parser.DefineVar("Unbalance", &pUnbalance);
  }
  parser.SetExpr(expression);
}
bool AnalyzerSoftAggreg::GetSeriesJSON(const string &serie, json &result) const {
  if(serie == "Frequency") {
    result["Frequency"] = pFrequency;
  } else if(serie == "Flag") {
    result["Flag"] = pFlag;
  } else if(serie == "Unbalance") {
    result["Unbalance"] = pUnbalance;
  } else if(serie == "Voltage") {
    json Tmp = json::array();
    for(uint8_t n = 0; n < pVoltageChannels; n++) Tmp.push_back(pVoltage[n]);
    result["Voltage"] = Tmp;
  } else if(serie == "Current") {
    json Tmp = json::array();
    for(uint8_t n = 0; n < pCurrentChannels; n++) Tmp.push_back(pCurrent[n]);
    result["Current"] = Tmp;
  } else if(serie == "Active_Power") {
    json Tmp = json::array();
    for(uint8_t n = 0; n < pVoltageChannels; n++) Tmp.push_back(pActive_Power[n]);
    result["Active_Power"] = Tmp;
  } else if(serie == "Reactive_Power") {
    json Tmp = json::array();
    for(uint8_t n = 0; n < pVoltageChannels; n++) Tmp.push_back(pReactive_Power[n]);
    result["Reactive_Power"] = Tmp;
  } else if(serie == "Active_Energy") {
    json Tmp = json::array();
    for(uint8_t n = 0; n < pVoltageChannels; n++) Tmp.push_back(pActive_Energy[n]);
    result["Active_Energy"] = Tmp;
  } else if(serie == "Reactive_Energy") {
    json Tmp = json::array();
    for(uint8_t n = 0; n < pVoltageChannels; n++) Tmp.push_back(pReactive_Energy[n]);
    result["Reactive_Energy"] = Tmp;
  } else if(serie == "Apparent_Power") {
    json Tmp = json::array();
    for(uint8_t n = 0; n < pVoltageChannels; n++) Tmp.push_back(pApparent_Power[n]);
    result["Apparent_Power"] = Tmp;
  } else if(serie == "Power_Factor") {
    json Tmp = json::array();
    for(uint8_t n = 0; n < pVoltageChannels; n++) Tmp.push_back(pPower_Factor[n]);
    result["Power_Factor"] = Tmp;
  } else if(serie == "Voltage_THD") {
    json Tmp = json::array();
    for(uint8_t n = 0; n < pVoltageChannels; n++) Tmp.push_back(pVoltage_THD[n]);
    result["Voltage_THD"] = Tmp;
  } else if(serie == "Current_THD") {
    json Tmp = json::array();
    for(uint8_t n = 0; n < pCurrentChannels; n++) Tmp.push_back(pCurrent_THD[n]);
    result["Current_THD"] = Tmp;
  } else if(serie == "Phi") {
    json Tmp = json::array();
    for(uint8_t n = 0; n < pVoltageChannels; n++) Tmp.push_back(pPhi[n]);
    result["Phi"] = Tmp;
  } else if(serie == "Voltage_Phase") {
    json Tmp = json::array();
    for(uint8_t n = 0; n < pVoltageChannels; n++) Tmp.push_back(pVoltage_Phase[n]);
    result["Voltage_Phase"] = Tmp;
  } else if(serie == "Voltage_Harmonics") {
    json Tmp1 = json::array();
    for(uint8_t n = 0; n < pVoltageChannels; n++) {
      json Tmp2 = json::array();
      for(uint8_t i = 0; i < 50; i++) Tmp2.push_back(VoltageHarmonic(n, i));
      Tmp1.push_back(Tmp2);
    }
    result["Voltage_Harmonics"] = Tmp1;
  } else if(serie == "Current_Harmonics") {
    json Tmp1 = json::array();
    for(uint8_t n = 0; n < pVoltageChannels; n++) {
      json Tmp2 = json::array();
      for(uint8_t i = 0; i < 50; i++) Tmp2.push_back(CurrentHarmonic(n, i));
      Tmp1.push_back(Tmp2);
    }
    result["Current_Harmonics"] = Tmp1;
  } else if(serie == "Power_Harmonics") {
    json Tmp1 = json::array();
    for(uint8_t n = 0; n < pVoltageChannels; n++) {
      json Tmp2 = json::array();
      for(uint8_t i = 0; i < 50; i++) Tmp2.push_back(PowerHarmonic(n, i));
      Tmp1.push_back(Tmp2);
    }
    result["Power_Harmonics"] = Tmp1;
  } else {
    return false;
  }
  return true;
}
// ---------------------------------------------------------------------------------------------------------------------