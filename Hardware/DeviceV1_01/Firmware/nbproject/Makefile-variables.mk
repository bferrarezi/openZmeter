#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=arm-none-eabi-Linux
CND_ARTIFACT_DIR_Debug=dist/Debug/arm-none-eabi-Linux
CND_ARTIFACT_NAME_Debug=1phase_b
CND_ARTIFACT_PATH_Debug=dist/Debug/arm-none-eabi-Linux/1phase_b
CND_PACKAGE_DIR_Debug=dist/Debug/arm-none-eabi-Linux/package
CND_PACKAGE_NAME_Debug=1phaseb.tar
CND_PACKAGE_PATH_Debug=dist/Debug/arm-none-eabi-Linux/package/1phaseb.tar
# Release configuration
CND_PLATFORM_Release=GNU-Linux
CND_ARTIFACT_DIR_Release=dist/Release/GNU-Linux
CND_ARTIFACT_NAME_Release=1phase_b
CND_ARTIFACT_PATH_Release=dist/Release/GNU-Linux/1phase_b
CND_PACKAGE_DIR_Release=dist/Release/GNU-Linux/package
CND_PACKAGE_NAME_Release=1phaseb.tar
CND_PACKAGE_PATH_Release=dist/Release/GNU-Linux/package/1phaseb.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
